#!/bin/bash

#=========== Bilgiler ========================================================#
# isim:     zahralik
# tanım:    rsync temelli yedekleme yapan bir BASH betiği
# yazar:    Numan Demirdöğen <if.gnu.linux@gmail.com>
# tarih:    03/04/2013
# sürüm:    6.0
# kullanım: bash zahralik.sh
# yardım:   bash zahralik.sh -y

#=========== Lisans ==========================================================#
# Copyright © 2013-2016 Numan Demirdöğen
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#=========== Temel Değişkenler ===============================================#
# Yedeğini almayı istediğiniz dizinleri tanımladığınız değişken.
# Dizinler arasında bir kararter boşluk bırakın ve
# dizin sonuna '/' ekleyin (tınak işareti olmadan).
# Örnek: hasat=("/etc/" "/home/ubuntu/")
hasat=()

# Yedeklerin hangi dizinde tutulacağını tanımladığınız değişken.
# Dizin, betiğin çalıştığı disk bölümünden farklı bir bölümdeyse,
# betiği çalıştırmadan önce o bölümü bağladığınızdan emin olun.
# Dizin sonuna '/' ekleyin.
# Örnek: "/media/kullanıcı_adı/disk_bölümü/yedek/"
ambar=""

# Birikimli yedek alınıp alınmayacağını tanımladığınız değişken.
# Birikimli yedek almak istediğiniz takdirde daha önce yedeği alınmış dizin ve
# dosyalarda değişikliğe uğramış dizin ve dosyalar ile yeni oluşturulan dizin
# ve dosyalar alacağınız yeni yedeğe eklenirken, değişmemiş olanlar ise yeni
# yedeğe eski yedekten sabit bağ (hard link ) olarak kopyalanır.
# Birikimli yedek alınmayacaksa 'hayır' yazın.
birikimli="evet"

# Yedeği şifrelerken kullanılacak GPG anahtarının kullanıcı adı.
# Eğer alınan yedeğin şifrelenmesini istemiyorsanız boş bırakınız.
ad=""

# BU NOKTADAN SONRASINI DEĞİŞTİRMENİZ ÖNERİLMEZ!

#=========== Rsync Seçenekleri ===============================================#
# Rsync için kullanılacak seçenekler.
# -a: arşiv kipi; -rlptgoD (-H'sız) ile aynı
# -r: dizinlerin alt dizinlerine inilir
# -l: sembolik bağları sembolik bağ olarak kopyalar
# -p: izinler korunur
# -t: zamanlar korunur
# -g: grup korunur
# -o: sahip korunur (sadece root)
# -D: aygıtlar korunur (sadece root)
# -q: hatalar dışında bilgi verilmez
# -z: aktarım sırasında dosya verisi sıkıştırılır
# Ayrıntılı bilgi için rsync el kitabına bakınız.
# Bu seçenekleri değiştirmeniz önerilmez.
secenekler="-azq"

# Yedeği alınacak dizinler içerisinde yedeği alınmayacak
# dizin ve dosyaları içeren listenin tanımlandığı değişken.
# --exclude-from seçeneğiyle ilgili ayrıntılı bilgi için
# rsync el kitabındaki ilgili değişkenin açıklamasına ve
# "Süzme Kuralları"na bakınız.
sap_saman="--exclude-from=elek.list"

#=========== Betik Değişkenleri ==============================================#
# Betiğin adı
betik="$(basename "$0")"

# Son yedeğin tarihiyle karşılaştırmak için değişken
tarih="$(date +%y%m%d)"

# Rsync çıktısının tutulacağı dosya
gunluk_rsync="${tarih}_rsync.log"

# Hataların yazıldığı dosya
kara_kitap="${tarih}_debug.log"

# Betiğin olduğu dizin
baslangic_dizini="$(pwd)"

# son satırı veren değişken
declare -i lastno

# Renkler
rn="\033[0m" # renksiz
ry="\033[1;32m" # yeşil
rk="\033[1;31m" # kırmızı

#=========== Betiğin Kullanımını Anlatan Fonksiyon ===========================#
kullanim ()
{
	cat << EOF

	kullanım: $0 [SEÇENEKLER] [DEĞER]...
	[-i] [gpg kullanıcı adı]
	[-h] [yedeklenecek dizin]
	[-a] [hedef dizin]
	[-b] [evet/hayir]
	[-s] [rsync seçenekleri]
	[-c]
	[-y]
	[-g]
	[-l]

	SEÇENEKLER:
	-i: GPG şifrenizi oluştururken tanımladığınız kullanıcı adını giriniz.
	-h: Yedeğini almak istediğiniz dizinleri, aralarında bir boşluk
	    ve dizin sonlarında / olacak şekilde giriniz.
	-a: Yedeklerin tutulacağı dizini sonunda / olacak şekilde giriniz.
	-b: Birikimli yedek al (evet/hayir)
	-s: Rsync için kullanmak istediğiniz seçenekleri giriniz.
	    Ayrıntılı bilgi için \`man rsync\`
	-c: Alınan son yedeği çıkartmak için kullanınız.
	-y: Yardım belgesini görüntüler.
	-g: Garanti koşullarını görüntüler.
	-l: Lisans kolullarını görüntüler.

	Örnek:
	./zahralik -i kullanıcı_adı -h /etc -a /yedekler -b evet
EOF
}

#=========== Hataları Toplayan Fonksiyon =====================================#
# Debug ve hata çıktılarını normal çıktıdan ayır ve tanımlanan dosyaya yaz
hata_topla () {
	# Betik herhangi bir şekilde sonlandırıldığında,
	# hangi satırda ve hangi komut uygulanırken
	# sonlandırıldığını öğrenmek için $PS4 değişkeni atanmıştır.
	# BASH'in 4 sürümü ile trap fonksiyonu ne zaman tetiklenirse
	# $LINENO değişkeni hep 1 değerini alacak şekilde değiştirildiği
	# için trap tetiklenmeden önceki son $LINENO değeri $lastno
	# değişkenine atanarak betiğin hangi satırda sonlandırıldığı
	# korunmuş olunuyor.
	# Anlatım: https://unix.stackexchange.com/questions/151771/
	PS4='DEBUG: $((lastno=$LINENO)) : '
	# `set -x` kullanıldığında, "trace" çıktılarını
	# atanan "file descriptor" değerine yazar. Betikte oluşan
	# herhangi bir hata çıktısını betiğin diğer çıktılarından
	# ayırmak için değişkene 2 (standart daha "file descriptor")
	# değeri verilmiştir.
	BASH_XTRACEFD=2
	# standart hata çıktılarını belirtilen dosyaya yaz
	exec 2>> "/tmp/${kara_kitap}"
}

#=========== Temel Değişkenleri Denetleyen Fonksiyon =========================#
# Temel değişkenlerin tanımlanıp tanımlanmadığını denetler.
temel_degiskenleri_denetle () {
	hata_topla
	# "trace" özelliğini aç
	set -x

	printf "%s\n" "Betiğin temel değişkenleri denetleniyor..."

	# $hasat() dizisindeki elelman sayısı
	hasat_eleman_sayisi="${#hasat[@]}"
	# $hasat() dizisi boşsa
	if [[ "${hasat_eleman_sayisi}" -eq 0 ]] ; then
		hata "\$hasat değişkeni tanımlanmadığı için betik kapatılıyor!"
	fi

	bosmudolumu=(ambar birikimli secenekler)
	# $bosmudolumu() dizisindeki her bir değişken için
	for eleman in "${bosmudolumu[@]}" ; do
		# değişkenin değerinin NULL olup olmadığına bak.
		# ${!değişken} gösterimi (indirect expansion), $bosmudolumu()
		# dizisindeki değişkenlerin değerlerini kullanmak için
		# ${!degisken} => ambar => /yedek
		if [[ "" == "${!eleman}" ]] ; then
			hata "${eleman} değişkenini tanımlanmadığı için betik kapatılıyor!"
		fi
	done

	printf "%s\n" "Değişken denetimi tamamlandı."

	# "trace" özelliğini kapat
	set -
}

#=========== ${hasat} dizininin mevcut olup olmadığına bakan fonksiyon =======#
hasat_varmi () {
	hata_topla
	set -x

	printf "%s\n" "Yedeklenecek dizinlerin mevcut olup olmadığına bakılıyor..."

	for (( i=0 ; i<"${hasat_eleman_sayisi}" ; i++ )) ; do
		# ${hasat} değişkeni için girilen dizin mevcut değilse...
		if [[ ! -e "${hasat[$i]}" ]] ; then
			hata "${hasat[$i]} dizini için bir yol tanımlamadığınız için çıkılıyor!"
		fi
	done

	set -
}

#=========== ${ambar} dizininin mevcut olup olmadığına bakan fonksiyon =======#
ambar_varmi () {
	hata_topla
	set -x

	printf "%s\n" "Yedeklerin tutulacağı dizinin mevcut olup olmadığına bakılıyor..."

	# ${ambar} değişkeni için girilen dizin mevcut değilse...
	if [[ ! -e "${ambar}" ]] ; then
		hata "\$ambar dizini mevcut olmadığı için betik kapatılıyor!"
	fi

	set -
}

#=========== $hasat ve $ambar için son karakterin / olduğunu doğrula =========#
# Yedeklenecek dizinlerin ve yedekleri barındıracak dizinin son karakterini
# bulup '/' karakteriyle karşılaştıran fonksiyon.
# Son karakter BASH'in ${parametre:konum:uzunluk} özelliği
# (Substring Expansion) kullanılarak elde edilmekte.
# Anlatım: http://wiki.bash-hackers.org/syntax/pe#substring_expansion
son_karakter_ne () {
	hata_topla
	set -x

	# parametre = $dizin  : değişkeninin değerini al
	# konum     = -1      : değişkenin son karakterinden başla
	# uzunluk   = 1       : ilk karakteri al
	ambar_son_karakter=${ambar: -1:1}
	for (( i=0 ; i<"${hasat_eleman_sayisi}" ; i++ )) ; do
		hasat_son_karakter[$i]=${hasat[$i]: -1:1}
		if [[ "${hasat_son_karakter[$i]}" != "/" ]] ; then
			hata "${hasat[$i]} dizini '/' ile bitmediği için betik kapatılıyor!"
		fi
	done

	if [[ "${ambar_son_karakter}" != "/" ]] ; then
		hata "${ambar} dizini '/' ile bitmediği için betik kapatılıyor!"
	fi

	set -
}

#=========== ${ambar} dizini için yazma iznine bakan fonksiyon ==============#
# ${ambar} değişkeni için tanımlanan dizin mevcut değilse
# bu dizini barındıran dizinin hangi izinlere sahip
# olduğuna bakıyoruz.
baglanti_icin_izni_varmi () {
	hata_topla
	set -x

	# "IFS=/" = Kelime ayrımını '/' karakterine göre yap
	# "set -f" = Dosya adı açılımını (globbing) kapat
	# "set -- $ambar" = Parametreleri (positional parameter) $ambar'a ata
	# "echo $#" = Kaç tane parametre olduğunu bas
	# Bu işlem alt süreç olarak gerçekleştiriliyor ki
	# betiğin tamamını etkilemesin.
	adet=$(IFS=/; set -f; set -- ${ambar}; echo $#)

	# / => bir elemanlı ∴ ${baglanti}=${ambar}.
	# /mnt ya da /mnt/ => iki elemanlı ∴ ${baglanti}=${ambar}
	# /mnt/yedek ya da /mnt/yedek/ = üç elemanlı ∴ ${baglanti}=/mnt
	# /mnt/abc/yedek ya da /mnt/abc/yedek/ = dört elemanlı ∴ ${baglanti}=/mnt/abc
	if [[ "${adet}" -ge 3 ]] ; then
		# BASH'in ${parametre%biçim} (Substring Removal) özelliği
		# kullanılarak $ambar dizinini barındıran dizin elde edilir.
		# Anlatım: http://wiki.bash-hackers.org/syntax/pe#substring_removal
		baglanti_yolu=${ambar%/*/*}
	else
		baglanti_yolu="${ambar}"
	fi

	printf "%s\n" "${baglanti_yolu} dizinine yazmak için gerekli izinlerin olup olmadığına bakılıyor..."

	# ${baglanti} dizinine ait okuma,yazma,çalıştırma
	# izinlerini ve bu dizinin hangi kullanıcı ve
	# grupta olduğuna dair bilgileri tutan değişken
	mapfile -t baglanti_yolu_bilgi < <(stat -L -c "%a %U %G" "${baglanti_yolu}")

	# ${baglanti} değişkenindeki okuma,yazma ve
	# çalıştırma izinlerine dair bilgiyi tutan
	# değişken. 755 gibi bir değere sahiptir.
	baglanti_yolu_izin=${baglanti_yolu_bilgi[0]}

	# ${baglanti} dizininin hangi kullanıcıya ait
	# olduğuna dair bilgiyi tutan değişken.
	baglanti_yolu_sahip=${baglanti_yolu_bilgi[1]}

	# ${baglanti} dizininin hangi gruba ait
	# olduğuna dair bilgiyi tutan değişken.
	baglanti_yolu_heyet=${baglanti_yolu_bilgi[2]}

	# Kullanıcının dahil olduğu gruplara ait
	# bilgiyi tutan değişken.
	mapfile -t benim_heyetler < <(groups "$(whoami)")

	for eleman in "${benim_heyetler[@]}" ; do
		if [[ "${eleman}" == "${baglanti_yolu_heyet}" ]] ; then
			heyetim="${eleman}"
		fi
	done

	# Root isek...
	if [[ "$(whoami)" == "root" ]] ; then
		printf "%s\n" "${baglanti_yolu} dizini için gerekli izinlere sahipsiniz."
	# Dizinin sahibi ve yazma iznimiz varsa...
	elif [[ "$(whoami)" == "${baglanti_yolu_sahip}" ]] && [[ "${baglanti_yolu_izin}" -ge 300 ]] ; then
		printf "%s\n" "${baglanti_yolu} dizini için gerekli izinlere sahipsiniz."
	# Dizin grubunda isek ve yazma iznimiz varsa...
	elif [[ "${heyetim}" == "${baglanti_yolu_heyet}" ]] && [[ "${baglanti_yolu_izin}" -ge 30 ]] ; then
		printf "%s\n" "${baglanti_yolu} dizini için gerekli izinlere sahipsiniz."
	else # hiç biri yoksa...
		kusur "${baglanti_yolu} dizinine yazmak için gerekli izinlere sahip değilsiniz!"
		kusur "chmod 755 -R ${baglanti_yolu} komutunu deneyin."
		exit 1
	fi

	set -
}

#=========== ${ambar} dizini için yazma iznine bakan fonksiyon ==============#
# ${ambar} değişkeni için girilen dizin için, betik root olarak
# çalıştırılmıyorsa, gerekli olan yazma (w) ve çalıştırma (x)
# izinlerinin veya kullanıcının bu izinlere sahip bir grupta
# olup olmadığına bakılır.
ambar_icin_izin_varmi () {
	hata_topla
	set -x

	printf "%s\n" "${ambar} dizinine yazmak için gerekli izinlerin olup olmadığına bakılıyor..."

	# ${baglanti} dizinine ait okuma,yazma,çalıştırma
	# izinlerini ve bu dizinin hangi kullanıcı ve
	# grupta olduğuna dair bilgileri tutan değişken
	mapfile -t ambar_bilgi < <(stat -L -c "%a %U %G" "${ambar}")

	# ${baglanti} değişkenindeki okuma,yazma ve
	# çalıştırma izinlerine dair bilgiyi tutan
	# değişken. 755 gibi bir değere sahiptir.
	ambar_izin=${ambar_bilgi[0]}

	# ${baglanti} dizininin hangi kullanıcıya ait
	# olduğuna dair bilgiyi tutan değişken.
	ambar_sahip=${ambar_bilgi[1]}

	# ${baglanti} dizininin hangi gruba ait
	# olduğuna dair bilgiyi tutan değişken.
	ambar_heyet=${ambar_bilgi[2]}

	# Kullanıcının dahil olduğu gruplara ait
	# bilgiyi tutan değişken.
	mapfile -t benim_heyetler < <(groups "$(whoami)")

	for eleman in "${benim_heyetler[@]}"; do
		if [[ "${eleman}" == "${ambar_heyet}" ]] ; then
			heyetim="${eleman}"
		fi
	done

	# Root isek...
	if [[ "$(whoami)" == "root" ]] ; then
		printf "%s\n" "${ambar} dizini için gerekli izinlere sahipsiniz."
	# Dizinin sahibi ve yazma iznimiz varsa...
	elif [[ "$(whoami)" == "${ambar_sahip}" ]] && [[ "${ambar_izin}" -ge 300 ]] ; then
		printf "%s\n" "${ambar} dizini için gerekli izinlere sahipsiniz."
	# Dizin grubunda isek ve yazma iznimiz varsa...
	elif [[ "${heyetim}" == "${ambar_heyet}" ]] && [[ "${ambar_izin}" -ge 30 ]] ; then
		printf "%s\n" "${ambar} dizini için gerekli izinlere sahipsiniz."
	else # hiç biri yoksa...
		kusur "${ambar} dizinine yazmak için gerekli izinlere sahip değilsiniz!"
		kusur "chmod 755 -R ${ambar} komutunu deneyin."
		exit 1
	fi

	set -
}

#=========== Alınan son yedeği bulan fonksiyon ===============================#
son_zahrayi_bul () {
	hata_topla
	set -x

	# Alınan son yedeği tutan değişkeni tanımla
	son_zahra=""

	# Son şifreli yedeğin tarihini bul.
	# Hiçbir işlem için ls ve grep komutunu kullanmayınız
	# Anlatım: http://mywiki.wooledge.org/ParsingLs
	# find ile dosya bulunur;
	# printf ile dosyanın son düzenlenme tarihi yazdırılır;
	# '.' ile çıktı ayrıştırılıp 3. eleman alınır;
	# rakamlara göre sıralanır ve ilk sıradaki alınır.
	gpgli=$(find "${ambar}" -name '*.bz2.gpg' -type f -printf '%t %f\n' \
		| cut -d'.' -f 3 | sort -nr | head -1)

	# Son şifresiz yedeğin tarihini bul.
	tarli=$(find "${ambar}" -name '*.bz2' -type f -printf '%t %f\n' \
		| cut -d'.' -f 3 | sort -nr | head -1)

	# Şifreli bir yedeğimiz varsa...
	if [[ "${gpgli}" != "" ]] && [[ "${tarli}" = "" ]] ; then
		son_zahra="${gpgli}"
		son_zahra_yolu="${ambar}yedek.${son_zahra}.tar.bz2.gpg"
		# 160530 biçimindeki $son_zahrayı 16/05/30 biçimine dönüştür
		printf "${ry}%s${rn}\n" "Son yedeğiniz ${gpgli:0:2}/${gpgli:2:2}/${gpgli:4:2} tarihinde alınmış."
	# tarli bir yedeğimiz varsa...
	elif [[ "${tarli}" != "" ]] && [[ "${gpgli}" = "" ]] ; then
		son_zahra="${tarli}"
		son_zahra_yolu="${ambar}yedek.${son_zahra}.tar.bz2"
		printf "${ry}%s${rn}\n" "Son yedeğiniz ${tarli:0:2}/${tarli:2:2}/${tarli:4:2} tarihinde alınmış."
	# Her ikisi de mevcutsa, tarihleri karşılaştır ve güncel olanı seç.
	elif [[ "${gpgli}" != "" ]] && [[ "${tarli}" != "" ]] ; then
		if [[ "${gpgli}" -ge "${tarli}" ]] ; then
			son_zahra="${gpgli}"
			son_zahra_yolu="${ambar}yedek.${son_zahra}.tar.bz2.gpg"
			printf "${ry}%s${rn}\n" "Son yedeğiniz ${gpgli:0:2}/${gpgli:2:2}/${gpgli:4:2} tarihinde alınmış."
		else
			son_zahra="${tarli}"
			son_zahra_yolu="${ambar}yedek.${son_zahra}.tar.bz2"
			printf "${ry}%s${rn}\n" "Son yedeğiniz ${tarli:0:2}/${tarli:2:2}/${tarli:4:2} tarihinde alınmış."
		fi
	else # ikisi de yoksa
		printf "${rk}%s${rn}\n" "Daha önce alınmış bir yedek bulunamadı!"
		printf "%s\n" "Normal yedek almaya geçiliyor."
		son_zahra="yok"
	fi

	set -
}

#=========== Yedeğin güncel olup olmadığını karşılaştıran fonksiyon ==========#
yedek_guncelmi () {
	hata_topla
	set -x

	printf "%s\n" "Yedeğin güncel olup olmadığına bakılıyor..."

	# Alınan son yedeğin tarihi betiği çalıştırdığımız tarihe eşitse
	if [[ "${son_zahra}" == "${tarih}" ]] ; then
		printf "${ry}%s${rn}\n" "Yedeğiniz günceldir. Çıkılıyor."
		exit 0
	else
		printf "%s\n" "Yedeğiniz güncel değildir. Yedekleme işlemine devam ediliyor."
	fi

	set -
}

#=========== Alınan son yedeği çıkartmak için izin olup olmadığını denetle ===#
son_yedegi_cikarmak_icin_izin_varmi () {
	hata_topla
	set -x

	cikarma_izni_var=""

	mapfile -t son_zahra_bilgi < <(stat -L -c "%a %U %G" "${son_zahra_yolu}")

	# ${baglanti} değişkenindeki okuma,yazma ve
	# çalıştırma izinlerine dair bilgiyi tutan
	# değişken. 755 gibi bir değere sahiptir.
	son_zahra_izin=${son_zahra_bilgi[0]}

	# ${baglanti} dizininin hangi kullanıcıya ait
	# olduğuna dair bilgiyi tutan değişken.
	son_zahra_sahip=${son_zahra_bilgi[1]}

	# ${baglanti} dizininin hangi gruba ait
	# olduğuna dair bilgiyi tutan değişken.
	son_zahra_heyet=${son_zahra_bilgi[2]}

	# Kullanıcının dahil olduğu gruplara ait
	# bilgiyi tutan değişken.
	son_zahra_heyetler=$(groups "$(whoami)")

	for son_zahra_eleman in "${son_zahra_heyetler[@]}"; do
		if [[ "${son_zahra_eleman}" == "${son_zahra_heyet}" ]] ; then
			son_zahra_heyetim="${son_zahra_eleman}"
		fi
	done

	# Root isek...
	if [[ "$(whoami)" == "root" ]] ; then
		printf "%s\n" "${son_zahra_yolu} dosyası için gerekli izinlere sahipsiniz."
		cikarma_izni_var="evet"
	# Dizinin sahibi ve yazma iznimiz varsa...
	elif [[ "$(whoami)" == "${son_zahra_sahip}" ]] && [[ "${son_zahra_izin}" -ge 300 ]] ; then
		printf "%s\n" "${son_zahra_yolu} dosyası için gerekli izinlere sahipsiniz."
		cikarma_izni_var="evet"
	# Dizin grubunda isek ve yazma iznimiz varsa...
	elif [[ "${son_zahra_heyetim}" == "${son_zahra_heyet}" ]] && [[ "${son_zahra_izin}" -ge 30 ]] ; then
		printf "%s\n" "${son_zahra_yolu} dosyası için gerekli izinlere sahipsiniz."
		cikarma_izni_var="evet"
	else # hiç biri yoksa...
		cikarma_izni_var="hayir"
		kusur "${son_zahra_yolu} dosyasını çıkarmak için gerekli izinlere sahip değilsiniz!"
		kusur "chmod 755 -R ${son_zahra_yolu} komutunu deneyin."
	fi

	set -
}

#=========== Birikimli yedekleme işini gerçekleştiren fonksiyon ==============#
# bir: rsync'in --link-dest parametresini değer olarak alan değişken.
# --link-dest=DİZİN: değişmediyse DİZİN'deki dosyalara sabit bağ (hard link)
zahrala_birikimli () {
	hata_topla
	set -x

	printf "%s\n" "Birikimli yedek almaya başlanıyor..."

	# $LINENO, betiğin durdurulduğu/iptal edildiği anda
	# işlem hangi satırda ise o satırın numarasını verir.
	# $BASH_COMMAND ise, betiğin durdurulduğu/iptal edildiği anda,
	# betik hangi komutu çalıştırıyorsa o komutu verir.
	# SIGHUP: betiği çalıştıran uçbirimin kapatıldığını ya da
	# kill -HUP pid komutunun uygulandığını ifade eder.
	# SIGINT: ctrl+c tuşlarına basıralarak
	# betiğin durdurulduğunu ifade eder.
	# SIGTERM: işlemin herhangi bir şekilde
	# durdurulduğunu ifade eder (kill pid).
	# SIGQUIT: ctrl+\ ve herhangi bir şekilde
	# klavyeden betiğe çıkma isteminde bulunulduğunu ifade eder.
	trap 'supur_a $lastno $LINENO "$BASH_COMMAND"' \
	     SIGHUP SIGINT SIGTERM SIGQUIT

	# Son alınan yedeği --link-dest'in değeri olarak ata
	birikimli_dizin="--link-dest="${ambar}${son_zahra}""

	# hasat değişkenindeki her bir dizin için
	for dizin in "${hasat[@]}" ; do
		# Yedeklerin geçici olarak tutulacağı dizinleri oluştur.
		printf "%s\n" "${ambar}${tarih}${dizin} dizini oluşturuluyor..."
		if ! mkdir -p "${ambar}${tarih}${dizin}" ;  then
		# mkdir'ın çıktı durumu sıfıra eşit değilse - başarısızsa -
			hata "Yedeklerin geçici olarak tutulacağı dizinler oluşturulamadı!"
		fi
		# Yedeği al
		printf "%s\n" "${dizin} dizini yedekleniyor..."
		rsync "${secenekler}" \
		      --delete-during \
		      --delete-excluded \
		      --stats -i \
		      --log-file="${ambar}${gunluk_rsync}" \
		      "${sap_saman}" \
		      "${birikimli_dizin}${dizin}" "${dizin}" "${ambar}${tarih}${dizin}"
	done

	printf "%s\n" "Yedekleme işlemi tamamlandı."

	set -
}

#=========== Yedekleme işini gerçekleştiren fonksiyon ========================#
# Birikimli yedek almayı istemediğiniz - birikimki="hayır" -
# takdirde çalışan fonksiyondur.
zahrala_d () {
	hata_topla
	set -x

	printf "%s\n" "Yedek almaya başlanıyor..."

	trap 'supur_a $lastno $LINENO "$BASH_COMMAND"' \
	     SIGHUP SIGINT SIGTERM SIGQUIT

	for dizin in "${hasat[@]}" ; do
		printf "%s\n" "${ambar}${tarih}${dizin} dizini oluşturuluyor..."
		mkdir -p "${ambar}${tarih}${dizin}" \
		      || hata "Yedeklerin geçici olarak tutulacağı dizinler oluşturulamadı!"
		# Yedeği al
		printf "%s\n" "${dizin} dizini yedekleniyor..."
		rsync "${secenekler}" \
		      --stats -i \
		      --log-file="${ambar}${gunluk_rsync}" \
		      "${sap_saman}" \
		      "${dizin}" "${ambar}${tarih}${dizin}"
	done

	printf "%s\n" "Yedekleme işlemi tamamlandı."

	set -
}

#=========== "$ambar" değişkeninde belirtilen dizini tarlayan fonksiyon ======#
yedegi_tarla () {
	hata_topla
	set -x

	printf "%s\n" "Alınan yedek tarlanıyor..."

	trap 'supur_b $lastno $LINENO $BASH_COMMAND' \
	     SIGHUP SIGINT SIGTERM SIGQUIT

	cd "${ambar}" || hata "${ambar} dizinine girilemedi!"

	tar -cjf yedek."${tarih}".tar.bz2 "${tarih}" \
	    || hata "Alınan yedek tarlanamadı!"
	printf "%s\n" "Yedeğin tarlanması tamamlandı."

	cd "${baslangic_dizini}" || hata "${baslangic_dizini} dizinine girilemedi!"

	set -
}

#=========== Zahrayı şifreleyen fonksiyon ====================================#
# --recipient: GPG şifre sahibinin adı.
# --use-agent: gpg-agent kullan.
# Betik - dolayısıyla gpg - sudo ile kullanıldığı vakit
# PIN Giriş Programından (pinentry) kaynaklanan bir hata yüzünden
# GPG şifresi alınamıyor. Bunu düzeltmek için bu parametrenin
# kullanılmasının yanı sıra $GNUPGHOME çevre değişkeninde
# belirttiğiniz GPG dizini içinde gpg-agent.conf ayar dosyasının
# oluşturulması gerekmektedir. Eğer çevre değişkeni atanmamışsa
# gpg dizini öntanımlı olarak ~/.gnupg dizinidir.
# Bu ayar dosyası içine
# pinentry-program /usr/bin/pinentry-curses
# no-grab
# default-cache-ttl 300
# yazmanız gerek.
yedegi_sifrele () {
	hata_topla
	set -x

	printf "%s\n" "Yedek sifreleniyor..."

	trap 'supur_c $lastno $LINENO $BASH_COMMAND' \
	     SIGHUP SIGINT SIGTERM SIGQUIT

	gpg -se --recipient "${ad}" "${ambar}"yedek."${tarih}".tar.bz2 \
	    || hata "Alınan yedek şifrelenemedi!"

	printf "%s\n" "Yedeğin şifrelenmesi tamamlandı."

	set -
}

#=========== Zahranın izinlerini değiştiren fonksiyon ========================#
yedegin_iznini_degistir () {
	hata_topla
	set -x

	printf "%s\n" "Yedeğin izinleri değiştiriliyor..."

	# Şifrelemeyi seçtiysek...
	if [[ "${ad}" != "" ]] ; then
		# Sadece dosya sahibi için okuma ve yazma hakkı verilir.
		chmod 600 "${ambar}"yedek."${tarih}".tar.bz2.gpg \
		      || kusur "Yedeğin izinleri değiştirilemedi!"
	else # şifrelemeyi seçmediysek...
		chmod 600 "${ambar}"yedek."${tarih}".tar.bz2 \
		      || kusur "Yedeğin izinler değiştirilemedi!"
	fi

	printf "%s\n" "Yedeğin izinleri değiştirildi."

	set -
}

#=========== Yedekleme için kullanılan geçici dizinleri silen fonksiyon ======#
temizle () {
	hata_topla
	set -x

	printf "%s\n" "Geçiçi dizinler siliniyor..."

	cd "${ambar}" || kusur "${ambar} dizinine girilemedi!"
	rm -rf "${tarih}" || kusur "Geçici ${tarih} dizini silinemedi!"

	if [[ "${birikimli}" == "evet" ]] ; then
		rm -rf "${son_zahra}" || kusur "Geçici ${son_zahra} dizini silinemedi!"
		cd "${baslangic_dizini}" || kusur "${baslangic_dizini} dizinine girilemedi!"
	fi

	printf "%s\n" "Geçici dizinler silindi."

	set -
}

#=========== Tarı silen fonksiyon ============================================#
tari_kaldir () {
	hata_topla
	set -x

	printf "%s\n" "Şifrelenmemiş tar kaldırılıyor..."

	cd "${ambar}" || kusur "${ambar} dizinine girilemedi!"
	rm yedek."${tarih}".tar.bz2 || kusur "Şifrelenmiş tar arşivi silinemedi!"

	printf "%s\n" "Şifrelenmemiş tar arşivi silindi."
	cd "${baslangic_dizini}" || kusur "${baslangic_dizini} dizinine girilemedi!"

	set -
}

#=========== Şifrelenmiş son yedeği çıkaran fonksiyon ========================#
# -o: çıkan dosyanın ismi
# --decrypt: çıkartılacak şifreli dosya
zahrayi_cikar () {
	hata_topla
	set -x

	printf "%s\n" "Alınan son yedek çıkartılıyor..."

	trap 'supur_d $lastno $LINENO $BASH_COMMAND' \
	     SIGHUP SIGINT SIGTERM SIGQUIT

	# Daha önceden şifreli bir yedek varsa...
	if [[ "${gpgli}" != "" ]] ; then
		gpg --recipient "${ad}" \
		    -o "${ambar}"yedek."${son_zahra}".tar.bz2 \
		    --decrypt "${ambar}"yedek."${son_zahra}".tar.bz2.gpg \
		    || hata "Şifrelenmiş yedek çıkartılamadı!"
		printf "%s\n" "Şifrelenmiş son yedek çıkartıldı."
		# Tar arşivini çıkar
		cd "${ambar}" || hata "${ambar} dizinine girilemedi!"
		tar -xjf yedek."${son_zahra}".tar.bz2 \
		    || hata "Tarlanmış yedek çıkartılamadı!"
		printf "%s\n" "Tarlanmış son yedek çıkartıldı."
		# Tar arşivini sil
		rm yedek."${son_zahra}".tar.bz2 \
		   || kusur "Tarlanmış yedek silinemedi!"
		printf "%s\n" "Tar arşivi silindi."
		cd "${baslangic_dizini}" || kusur "${baslangic_dizini} dizinine girilemedi!"
	# Ya da tar'lı bir yedek varsa...
	elif [[ "${tarli}" != "" ]] ; then
		cd "${ambar}" || hata "${ambar} dizinine girilemedi!"
		tar -xjf yedek."${son_zahra}".tar.bz2 \
		    || hata "Tarlanmış yedek çıkartılamadı!"
		printf "%s\n" "Tarlanmış son yedek çıkartıldı."
		cd "${baslangic_dizini}" || kusur "${baslangic_dizini} dizinine girilemedi!"
	else # ikisi de yoksa...
		kusur "Ne şifrelenmiş ne de tarlanmış bir yedek bulundu!"
	fi

	set -
}

#=========== İşlemlerdeki hataları ekrana yazan fonksiyon ====================#
hata () {
	# BASH'in ${parametre:-ifade} özelliğini kullanarak
	# hata() fonksiyonu için girilen ilk parametre (positional parameter)
	# atanmamış ya da NULL'sa 'Bilinmeyen hata!' ifadesini yazdır.
	# Anlatım: http://wiki.bash-hackers.org/syntax/pe#use_a_default_value
	printf "${rk}%s${rn}\n" "$betik: ${1:-Bilinmeyen hata!}"
	exit 1
}

kusur () {
 printf "${rk}%s${rn}\n" "$betik: ${1:-Bilinmeyen hata!}"
}

#=========== Temizlik fonksiyonları ==========================================#
# zahrala_birikimli ve zahrala_d fonksiyonları bir şekilde
#- ctrl+c gibi - durdurulursa bu fonksiyonlar tarafından oluşturulan
# ${tarih} dizinini silen fonksiyon. ${tarih} dizini silinmediği takdirde,
# durdurmanın ardından betik tekrar çalıştırılırsa yedek_guncelmi() fonksiyonu
# ${tarih} dizini mevcut olduğundan o gün için yedeğin güncel
# olduğunu sanacak ve betik yedek almadan çıkacaktır.
supur_a () {
	kusur "sonlandırılma isteği alındı!"
	kusur "satır: $1"
	kusur "komut: $BASH_COMMAND"
	kusur "${ambar}${tarih} dizini siliniyor..."
	cd "${ambar}" || kusur "${ambar} dizinine girilemedi!"
	rm -rdf "${tarih}" || kusur "${tarih} dizini silinemedi!"
	exit "$1"
}

# Herhangi bir şekilde yedeğin tarlama işlemi sonlandırılırsa
# geride kalan tar arşivini silen fonksiyon
supur_b () {
	kusur "sonlandırılma isteği alındı!"
	kusur "satır: $1"
	kusur "komut: $BASH_COMMAND"
	kusur "${ambar}${tarih} dizini ve yedek.${tarih}.tar.bz2 siliniyor..."
	cd "${ambar}" || kusur "${ambar} dizinine girilemedi!"
	rm -rdf "${tarih}" || kusur "${tarih} dizini silinemedi!"
	rm yedek."${tarih}".tar.bz2 || kusur "${tarih}.tar.bz2 dosyası silinemedi!"
	exit "$1"
}

# Herhangi bir şekilde yedeğin şifrelenmesi sonlandırılırsa
# geride kalan gpg dosyasını silen fonksiyon.
supur_c () {
	kusur "sonlandırılma isteği alındı!"
	kusur "satır: $1"
	kusur "komut: $BASH_COMMAND"
	kusur "${ambar}${tarih} dizini, yedek.${tarih}.tar.bz2 ve yedek.${tarih}.tar.bz2.gpg dosyası siliniyor..."
	cd "${ambar}" || kusur "${ambar} dizinine girilemedi!"
	rm -rdf "${tarih}" || kusur "${tarih} dizini silinemedi!"
	rm yedek."${tarih}".tar.bz2 || kusur "${tarih}.tar.bz2 dosyası silinemedi!"
	rm yedek."${tarih}".tar.bz2.gpg || kusur "${tarih}.tar.bz2.gpg dosyası silinemedi!"
	exit "$1"
}

# zahrada_b fonksiyonu için gerekli olan en güncel eski yedeği çıkartan
# zahrayi_cikar fonksiyonunun herhangi bir şekilde sonlandırılması sonucu
# şifreli yedeği çıkartırken geride kalan tar arşivini ve/veya tar arşivinin
# çıkartılması sonucu geride kalan ${son_zahra} dizinini silen fonksiyon.
supur_d () {
	kusur "sonlandırılma isteği alındı!"
	kusur "satır: $1"
	kusur "komut: $BASH_COMMAND"
	kusur "${ambar}${son_zahra} dizini siliniyor..."
	cd "${ambar}" || kusur "${ambar} dizinine girilemedi!"
	rm -rdf "${son_zahra}" || kusur "${son_zahra} dizini silinemedi!"
	exit "$1"
}

#=========== Garanti koşullarını gösteren fonksiyon ==========================#
garanti () {
	printf "%s\n" "GNU Genel Kamu Lisansı 3. sürümün garanti koşulları:"

	if [[ -e "${baslangic_dizini}"/LICENSE ]] ; then
		sed -n '258,278p;279q' "${baslangic_dizini}"/LICENSE
	else
		kusur "LICENSE dosyası bulunamadı!"
		kusur "https://www.gnu.org/licenses/gpl.html"
		kusur "bağlantısını kullanarak garanti kuşullarını öğrenebilirsiniz."
		exit 1
	fi
}

#=========== Lisans koşullarını gösteren fonksiyon ===========================#
lisans () {
	printf "%s\n" "GNU Genel Kamus Lisansı 3. sürümün lisans koşulları:"

	if [[ -e "${baslangic_dizini}"/LICENSE ]] ; then
		sed -n '59,256p;257q' "${baslangic_dizini}"/LICENSE
	else
		kusur "LICENSE dosyası bulunamadı!"
		kusur "https://www.gnu.org/licenses/gpl.html"
		kusur "bağlantısını kullanarak garanti kuşullarını öğrenebilirsiniz."
		exit 1
	fi
}

ise_koyul () {

#=========== Karşılama iletisi ===============================================#
printf "%s\n" "############################# Zahralık #############################"
printf "%s\n" "Rsync ile yedek alan bir Bash betiği"
printf "%s\n" "Zahralık 6.0, Copyright (C) 2013-2019 Numan Demirdöğen"

printf "%s\n" "Zahralık comes with ABSOLUTELY NO WARRANTY;"
printf "%s\n" "for details type 'zahralık -g'"
printf "%s\n" "This is free software, and you are welcome"
printf "%s\n" "to redistribute it under certain conditions;"
printf "%s\n" "type 'zahralık -l' for details."
printf "%s\n" ""

#========== Fonksiyonları uygula =============================================#
temel_degiskenleri_denetle
hasat_varmi
ambar_varmi
son_karakter_ne
baglanti_icin_izni_varmi
ambar_icin_izin_varmi
son_zahrayi_bul

if [[ "${son_zahra}" != "yok" ]] ; then
	yedek_guncelmi
	if [[ "${birikimli}" == "evet" ]] ; then
		son_yedegi_cikarmak_icin_izin_varmi
		if [[ "${cikarma_izni_var}" == "evet" ]] ; then
			zahrayi_cikar
			zahrala_birikimli
			yedegi_tarla
			if [[ "${ad}" != "" ]] ; then
				yedegi_sifrele
				yedegin_iznini_degistir
				temizle
				tari_kaldir
				printf "${ry}%s${rn}\n" "Yedeğiniz başarıyla alınmıştır."
				exit 0
			else
				yedegin_iznini_degistir
				temizle
				printf "${ry}%s${rn}\n" "Yedeğiniz başarıyla alınmıştır."
				exit 0
			fi
		else
			printf "%s" "Normal yedek alarak devam edilsin mi? (e/h):"
			read -r
			if [[ "${REPLY}" == "e" ]] ; then
				zahrala_d
				yedegi_tarla
				if [[ "${ad}" != "" ]] ; then
					yedegi_sifrele
					yedegin_iznini_degistir
					temizle
					tari_kaldir
					printf "${ry}%s${rn}\n" "Yedeğiniz başarıyla alınmıştır."
					exit 0
				else
					yedegin_iznini_degistir
					temizle
					printf "${ry}%s${rn}\n" "Yedeğiniz başarıyla alınmıştır."
					exit 0
				fi
			else
				hata "Yedek almadan çıkılıyor!"
			fi
		fi
	else
		zahrala_d
		yedegi_tarla
		if [[ "${ad}" != "" ]] ; then
			yedegi_sifrele
			yedegin_iznini_degistir
			temizle
			tari_kaldir
			printf "${ry}%s${rn}\n" "Yedeğiniz başarıyla alınmıştır."
			exit 0
		else
			yedegin_iznini_degistir
			temizle
			printf "${ry}%s${rn}\n" "Yedeğiniz başarıyla alınmıştır."
			exit 0
		fi
	fi
else
	zahrala_d
	yedegi_tarla
	if [[ "${ad}" != "" ]] ; then
		yedegi_sifrele
		yedegin_iznini_degistir
		temizle
		tari_kaldir
		printf "${ry}%s${rn}\n" "Yedeğiniz başarıyla alınmıştır."
		exit 0
	else
		yedegin_iznini_degistir
		temizle
		printf "${ry}%s${rn}\n" "Yedeğiniz başarıyla alınmıştır."
		exit 0
	fi
fi
}

# uçbirimde y,c,i,h,a,s parametrelerini okursan
while getopts "yci:h:a:b:s:gl" parametre; do
	case ${parametre} in
		y) kullanim ; exit 0;; # kullanım() fonksiyonunu çalıştır
		i) ad=${OPTARG};; # $ad değişkenine ata
		h) hasat=( "${OPTARG}" );;
		a) ambar=${OPTARG};;
		b) birikimli=${OPTARG};;
		s) secenekler=${OPTARG};;
		c) son_zahrayi_bul ; zahrayi_cikar ; exit 0 ;;
		g) garanti; exit 0;;
		l) lisans; exit 0;;
		# yanlış argüman verildiğinde kullanım fonksiyonunu çalıştır
		\?) printf "${rk}%s${rn}\n" "Geçersiz seçenek!" ; kullanim; exit 1 ;;
	esac
done

ise_koyul
